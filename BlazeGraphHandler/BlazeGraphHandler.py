'''
Created on 05.01.2017

@author: dwinter
'''
import rdflib
import logging

from rdflib.plugins.stores.sparqlstore import SPARQLUpdateStore

#from SPARQLWrapper.SmartWrapper import SPARQLWrapper2
import csv
NAMESPACE_SPARQL_PREFIX="""
        prefix owl: <http://www.w3.org/2002/07/owl#>
        prefix crm: <http://erlangen-crm.org/160714/>
        prefix sr: <http://ontologies.mpiwg-berlin.mpg.de/scholarlyRelations/>
        prefix skos: <http://www.w3.org/2004/02/skos/core#>
        """
OWL = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
CRM = rdflib.Namespace("http://erlangen-crm.org/160714/")
SR = rdflib.Namespace("http://ontologies.mpiwg-berlin.mpg.de/scholarlyRelations/")
SKOS = rdflib.Namespace("http://www.w3.org/2004/02/skos/core#")
BDS = rdflib.Namespace("http://www.bigdata.com/rdf/search#")

class BlazeGraphHandlerException(Exception):
    pass
    
    
class BlazeGraphStore(SPARQLUpdateStore):
    """Extends SPARQLUpdateStore 
        add a function to load graphs into the storage
    
    """  
    
    def __init__(self,queryEndpoint,defaultGraph=None, updateEndpoint = None , auth=None):
        
        if defaultGraph is None:
            logging.warn("if blazegraph is true, defaultgraph has to be set, updating will not work") 
        self.defaultGraph=defaultGraph
        SPARQLUpdateStore.__init__(self,queryEndpoint, update_endpoint=updateEndpoint, auth=auth)
        
        
    def load(self,iri,graph_uri=None):
        """
        :param iri iri: iri to the rdf file with the data to be uploaded
        :param uri grap_uri: (optional) uri of the named graph
        """
       
        if graph_uri is None:
            graph_uri = self.defaultGraph
       
        qs="LOAD <%s> into GRAPH <%s>"%(iri,graph_uri)
        res = self.update(qs)

        
        return res

    def query_to_cvs(self,fn,qs):

        res = self.query(qs)
        with open(fn,"w") as out:
            writer = csv.writer(out,dialect="excel")
            for r in res:
                writer.writerow(["%s"%s for s in r])

if __name__ == '__main__':
    
    bgh = BlazeGraphStore(queryEndpoint="http://dw2.mpiwg-berlin.mpg.de:10214/blazegraph/namespace/gmpg/sparql" )
    
    #bgh.load("file:///tmp/graph.rdf","http://TMP2")

    qs ="""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX sr: <http://ontologies.mpiwg-berlin.mpg.de/scholarlyRelations/>

    select distinct ?id ?n ?p2  where {
	    ?p3 sr:has_name/rdfs:label ?n.
        ?p3 sr:suggestSimilarbyDates ?p2.
  
  	    ?p3 sr:has_id/rdfs:label ?id .
} limit 100000000000
    
    """

    bgh.query_to_cvs("/tmp/names.csv",qs)
    
    
    
    

    