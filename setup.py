from distutils.core import setup

setup(
    name='BlazeGraphHandler',
    version='0.53',
    packages=["BlazeGraphHandler"],
    url='',
    license='',
    author='dwinter',
    author_email='dwinter@mpiwg-berlin.mpg.de',
    install_requires=[
          'rdflib',
      ],
    description=''
)
